angular.module('login', ['ui.bootstrap','ui.router','ngAnimate']);

angular.module('login').config(function($stateProvider) {

    /* Add New States Above */

    $stateProvider.state('home', {
        url: '/home',
        templateUrl: 'login/partial/login/login.html',
        controller: 'LoginCtrl'
    })
    .state('sidenav', {
        url: '/sidenav',
        templateUrl: 'login/partial/login/sidenav.html',
        controller: 'LoginCtrl'
    })
    .state('sidenav.dashboard', {
        url: '/dashboard',
        templateUrl: 'login/partial/login/dashboard.html',
        controller: 'LoginCtrl'
    })
    .state('sidenav.bplist', {
        url: '/bplist',
        templateUrl: 'login/partial/login/bplist.html',
        controller: 'LoginCtrl'
    })
    .state('sidenav.purchase', {
        url: '/purchase',
        templateUrl: 'login/partial/login/purchase.html',
        controller: 'LoginCtrl'
    });
});


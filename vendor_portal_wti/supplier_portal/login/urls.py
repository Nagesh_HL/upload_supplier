from django.conf.urls import url

from .views import login_check, bp_list, purchase_list, sort_purchase_list

urlpatterns = [
    url(r'^logincheck', login_check),
    url(r'^bplist', bp_list),
    url(r'^purchaselist', purchase_list),
    url(r'^sortpurchaselist', sort_purchase_list)
]